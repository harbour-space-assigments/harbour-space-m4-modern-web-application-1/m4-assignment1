<?php

namespace Tests\Unit;

use App\Models\Author;
use App\Models\Book;
use App\Models\User;
use Faker\Factory as FakerFactory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class BooksTest extends TestCase
{
    public function test_user_can_show_books()
    {
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('book.index'))->assertOk();
    }

    public function test_user_can_show_book()
    {
        $book = Book::factory()->createOne();
        $user = User::factory()->createOne();

        $this->actingAs($user)->get(route('book.show', $book->id))->assertOk();
    }

    public function test_browse_books_requires_authorization()
    {
        $this->get(route('book.create'))->assertRedirect(route('login'));
    }

    public function test_create_book_requires_authorization()
    {
        $this->get(route('book.create'))->assertRedirect(route('login'));
    }

    public function test_user_can_create_book()
    {
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('book.create'))->assertOk();
    }

    public function test_non_owner_cannot_edit_book()
    {
        $book = Book::factory()->createOne();
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('book.edit', $book->id))->assertStatus(403);
    }

    public function test_edit_book_requires_authorization()
    {
        $book = Book::factory()->createOne();
        $this->get(route('book.edit', $book->id))->assertRedirect(route('login'));
    }

    public function test_admin_can_edit_book()
    {
        $book = Book::factory()->createOne();
        $admin = User::factory()->createOne(['is_admin' => 1]);
        $this->actingAs($admin)->get(route('book.edit', $book->id))->assertOk();
    }

    public function test_owner_can_edit_book()
    {
        $book = Book::factory()->createOne();
        $this->actingAs($book->owner)->get(route('book.edit', $book->id))->assertOk();
    }

    public function test_delete_book_requires_authorization()
    {
        $book = Book::factory()->createOne();
        $this->get(route('book.delete', $book->id))->assertRedirect(route('login'));
    }

    public function test_non_owner_cannot_delete_book()
    {
        $book = Book::factory()->createOne();
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('book.delete', $book->id))->assertStatus(403);
    }

    public function test_owner_can_delete_book()
    {
        $book = Book::factory()->createOne();
        $this->actingAs($book->owner)->get(route('book.delete', $book->id))->assertOk();
    }

    public function test_admin_can_delete_book()
    {
        $book = Book::factory()->createOne();
        $admin = User::factory()->createOne(['is_admin' => true]);
        $this->actingAs($admin)->get(route('book.delete', $book->id))->assertOk();
    }

    public function test_non_owner_cannot_destroy_book()
    {
        $book = Book::factory()->createOne();
        $user = User::factory()->createOne();
        $this->actingAs($user)->post(route('book.destroy', $book->id))->assertStatus(403);
    }

    public function test_destroy_book_requires_authorization()
    {
        $book = Book::factory()->createOne();
        $this->post(route('book.destroy', $book->id))->assertRedirect(route('login'));
    }

    public function test_admin_can_destroy_book()
    {
        $book = Book::factory()->createOne();
        $admin = User::factory()->createOne(['is_admin' => true]);
        $this->actingAs($admin)->post(route('book.destroy', $book->id))->assertRedirect(route('book.index'));
    }

    public function test_users_can_store_the_book()
    {
        Storage::fake('public');
        $user = User::factory()->createOne();
        $book = Book::factory()->make();
        $files = collect(range(0, 5))->map(fn () => UploadedFile::fake()->image(FakerFactory::create()->word() . ".jpg"))->toArray();
        $descriptions = collect(range(0, 5))->map(fn () => FakerFactory::create()->sentence())->toArray();

        $data = array_merge($book->toArray(), [
            "images" => $files,
            "descriptions" => $descriptions,
        ]);

        $this->actingAs($user)->post(route('book.store'), $data)->assertRedirect(route('book.index'))->assertSessionHasNoErrors();

        foreach ($files as $file)
            Storage::disk('public')->assertExists($file->hashName());

    }

    public function test_owner_can_update_the_book()
    {
        Storage::fake('public');

        $book = Book::query()->first();
        $new_book = Book::factory()->make();

        $files = collect(range(0, 5))->map(fn () => UploadedFile::fake()->image(FakerFactory::create()->word() . ".jpg"))->toArray();
        $descriptions = collect(range(0, 5))->map(fn () => FakerFactory::create()->sentence())->toArray();

        $data = array_merge($new_book->toArray(), [
            "images" => $files,
            "descriptions" => $descriptions,
        ]);

        $this->actingAs($book->owner)->post(route('book.update', $book->id), $data)->assertRedirect(route('book.show', $book->id))->assertSessionHasNoErrors();

        foreach ($files as $file)
            Storage::disk('public')->assertExists($file->hashName());

    }

    protected function setUp(): void
    {
        parent::setUp();

        // Create at least one record for eah table
        User::factory()->createOne();
        Author::factory()->createOne();
        Book::factory()->createOne();
    }
}
