<?php

namespace Tests\Unit;

use App\Models\Author;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AuthorsTest extends TestCase
{
    public function test_user_can_show_authors()
    {
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('author.index'))->assertOk();
    }

    public function test_user_can_show_author()
    {
        $author = Author::factory()->createOne();
        $user = User::factory()->createOne();

        $this->actingAs($user)->get(route('author.show', $author->id))->assertOk();
    }

    public function test_browse_authors_requires_authorization()
    {
        $this->get(route('author.create'))->assertRedirect(route('login'));
    }

    public function test_create_author_requires_authorization()
    {
        $this->get(route('author.create'))->assertRedirect(route('login'));
    }

    public function test_user_can_create_author()
    {
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('author.create'))->assertOk();
    }

    public function test_non_creator_cannot_edit_author()
    {
        $author = Author::factory()->createOne();
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('author.edit', $author->id))->assertStatus(403);
    }

    public function test_edit_author_requires_authorization()
    {
        $author = Author::factory()->createOne();
        $this->get(route('author.edit', $author->id))->assertRedirect(route('login'));
    }

    public function test_admin_can_edit_author()
    {
        $author = Author::factory()->createOne();
        $admin = User::factory()->createOne(['is_admin' => 1]);
        $this->actingAs($admin)->get(route('author.edit', $author->id))->assertOk();
    }

    public function test_creator_can_edit_author()
    {
        $author = Author::factory()->createOne();
        $this->actingAs($author->creator)->get(route('author.edit', $author->id))->assertOk();
    }

    public function test_delete_author_requires_authorization()
    {
        $author = Author::factory()->createOne();
        $this->get(route('author.delete', $author->id))->assertRedirect(route('login'));
    }

    public function test_non_creator_cannot_delete_author()
    {
        $author = Author::factory()->createOne();
        $user = User::factory()->createOne();
        $this->actingAs($user)->get(route('author.delete', $author->id))->assertStatus(403);
    }

    public function test_creator_can_delete_author()
    {
        $author = Author::factory()->createOne();
        $this->actingAs($author->creator)->get(route('author.delete', $author->id))->assertOk();
    }

    public function test_admin_can_delete_author()
    {
        $author = Author::factory()->createOne();
        $admin = User::factory()->createOne(['is_admin' => true]);
        $this->actingAs($admin)->get(route('author.delete', $author->id))->assertOk();
    }

    public function test_non_creator_cannot_destroy_author()
    {
        $author = Author::factory()->createOne();
        $user = User::factory()->createOne();
        $this->actingAs($user)->post(route('author.destroy', $author->id))->assertStatus(403);
    }

    public function test_destroy_author_requires_authorization()
    {
        $author = Author::factory()->createOne();
        $this->post(route('author.destroy', $author->id))->assertRedirect(route('login'));
    }

    public function test_admin_can_destroy_author()
    {
        $author = Author::factory()->createOne();
        $admin = User::factory()->createOne(['is_admin' => true]);
        $this->actingAs($admin)->post(route('author.destroy', $author->id))->assertRedirect(route('author.index'));
    }

    public function test_users_can_store_the_author()
    {
        Storage::fake('public');
        $user = User::factory()->createOne();
        $author = Author::factory()->make();

        $this->actingAs($user)->post(route('author.store'), $author->toArray())->assertRedirect(route('author.index'))->assertSessionHasNoErrors();
    }

    public function test_creator_can_update_the_author()
    {
        Storage::fake('public');

        $author = Author::query()->first();
        $new_author = Author::factory()->make();

        $this->actingAs($author->creator)->post(route('author.update', $author->id), $new_author->toArray())->assertRedirect(route('author.show', $author->id))->assertSessionHasNoErrors();
    }

    protected function setUp(): void
    {
        parent::setUp();

        // Create at least one record for eah table
        User::factory()->createOne();
        Author::factory()->createOne();
    }
}
