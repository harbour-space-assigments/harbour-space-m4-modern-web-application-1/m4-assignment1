<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>HS Assigment</title>

    <!-- Loading third party fonts -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
    <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Loading main css file -->
    <link rel="stylesheet" href="style.css">

    <!--[if lt IE 9]>
    <script src="js/ie-support/html5.js"></script>
    <script src="js/ie-support/respond.js"></script>
    <![endif]-->

</head>

<body>

<div class="site-content">
    <div class="site-header">
        <div class="container">
            <a href="index.html" class="branding">
                <img src="images/logo.png" alt="" class="logo">
                <div class="logo-type">
                    <h1 class="site-title">60 min Project</h1></div>
            </a>

            <!-- Default snippet for navigation -->
            <div class="main-navigation">
                <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                <ul class="menu">

                </ul> <!-- .menu -->
            </div> <!-- .main-navigation -->

            <div class="mobile-navigation"></div>

        </div>
    </div> <!-- .site-header -->

    <div class="hero" data-bg-image="images/banner.png">
        <div class="container">
            <form action="weather" class="find-location">
                @csrf()
                <input name="q" type="text" placeholder="Find your location..." value="{{old('q', $q ?? "Spain")}}">
                <input type="submit" value="Find">
            </form>

        </div>
    </div>
    <div class="forecast-table">
        <div class="container">
            <div class="forecast-container">
                <p class="num" style='padding: 120px 20px; color: #bfc1c8; font-family: "Roboto", "Open Sans", sans-serif;  font-size: 25px; font-weight: 300; line-height: 1.5; background: #1e202b;' >
                    OPPS! - Something went wrong, check your search query and try again.
                </p>
            </div>
        </div>
    </div>

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <form action="#" method="post" class="subscribe-form">
                        <input type="text" placeholder="" style="visibility: hidden">
                    </form>
                </div>
                <div class="col-md-3 col-md-offset-1">
                </div>
            </div>

            <p class="colophon">Copyright 2021 Harbour.Space. Designed by Adel in 60 min, All rights reserved</p>
        </div>
    </footer> <!-- .site-footer -->
</div>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>

</body>

</html>
