@php
    function toC($k){return $k -273.15; }
    function getDir($th){
        if($th >= 0 && $th <90)
            return "East";
        if($th >= 90 && $th < 180)
            return "North";
        if($th >= 180 && $th <270)
            return "West";
        if($th >= 270 && $th <360)
            return "South";

        return null;
    }

    function display_arrow($column){
        $sorted_column = request()->input('sort_by');
        $sorted_order = request()->input('sort_order');

        $params = "sort_by=${column}&sort_order=" . ($sorted_order == "asc" ? "desc": "asc");

        if($column == $sorted_column){
            if($sorted_order == "asc")
                return '<a href= "'. '/weather-table?' . $params   .'">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"/>
                </svg>
             </a>' ;

            else
                return '<a href= "'. '/weather-table?' . $params   .'">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z"/>
                    </svg>
             </a>' ;
        }
        else{
                return '<a href= "'. '/weather-table?' . $params   .'">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-up" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"/>
                </svg>
             </span>' ;
        }
    }

@endphp
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>HS Assigment</title>

    <!-- Loading third party fonts -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
    <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Loading main css file -->
    <link rel="stylesheet" href="style.css">

    <!--[if lt IE 9]>
    <script src="js/ie-support/html5.js"></script>
    <script src="js/ie-support/respond.js"></script>
    <![endif]-->

</head>
<body>


<div class="site-content">
    <div class="site-header">
        <div class="container">
            <a href="/" class="branding">
                <img src="images/logo.png" alt="" class="logo">
                <div class="logo-type">
                    <h1 class="site-title">HS Assigment</h1>
                </div>
            </a>

            <!-- Default snippet for navigation -->
            <div class="main-navigation">
                <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                <ul class="menu">
                    <li class="menu-item current-menu-item"><a href="/">Home</a></li>
                    <li class="menu-item"><a href="/weather-table">Weather Table </a></li>

                </ul> <!-- .menu -->
            </div> <!-- .main-navigation -->

            <div class="mobile-navigation">
                <ul class="menu">
                    <li class="menu-item current-menu-item"><a href="index.html">Home</a></li>
                    <li class="menu-item"><a href="/weather-table">Weather Table</a></li>
                </ul>
            </div>

        </div>
    </div>

    <div class="hero" data-bg-image="images/banner.png" style="padding-bottom: 20px;     min-height: 160px;">
        <div class="container">
            <form action="weather-table" class="find-location">
                @csrf()
                <input name="q" type="text" placeholder="Find your location..." value="{{old('q', $q ?? "Spain")}}">
                <input type="submit" value="Find">
            </form>

        </div>
    </div>
    <div class="forecast-table">
        <div class="container">
            <div class="table">
                <div class="table-header">
                    <div class="header__item"><a id="name" class="filter__link" href="#">Day {!! display_arrow('day') !!}</a></div>
                    <div class="header__item"><a id="wins" class="filter__link filter__link--number" href="#">Day {!! display_arrow('temperature-day') !!}</a></div>
                    <div class="header__item"><a id="wins" class="filter__link filter__link--number" href="#">Night {!! display_arrow('temperature-night') !!}</a></div>
                    <div class="header__item"><a id="wins" class="filter__link filter__link--number" href="#">Clouds {!! display_arrow('clouds') !!}</a></div>
                    <div class="header__item"><a id="wins" class="filter__link filter__link--number" href="#">Rain {!! display_arrow('rain') !!}</a></div>
                </div>
                <div class="table-content">
                    @foreach($daily as $day)
                        <div class="table-row">
                            <div class="table-data">{{date('D d-m-Y', $day->dt)}}</div>
                            <div class="table-data">{{round(toC($day->temp->day), 1)}} <sup>o</sup>C</div>
                            <div class="table-data">{{round(toC($day->temp->night), 1)}} <sup>o</sup>C</div>
                            <div class="table-data">{{@$day->clouds ?? "-"}} </div>
                            <div class="table-data">{{isset($day->rain) ? ($day->rain * 100 . "%")  : "-"}} </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <form action="#" method="post" class="subscribe-form">
                        <input type="text" placeholder="" style="visibility: hidden">
                    </form>
                </div>
                <div class="col-md-3 col-md-offset-1">
                </div>
            </div>

            <p class="colophon">Copyright 2021 Harbour.Space. Designed by Adel in 60 min, All rights reserved</p>
        </div>
    </footer> <!-- .site-footer -->
</div>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>

</body>
</html>
