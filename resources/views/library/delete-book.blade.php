@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Are you sure that you want to delete the book?</h4>
                                <h5><a href="{{route('book.show', $book->id)}}"> {{$book->title }}</a></h5>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form action="{{route('book.destroy', $book->id)}}" method="post">
                                @csrf()
                                <button type="submit" class="btn btn-danger">Delete</button>
                                <button onclick="window.location.href='{{route('book.index')}}'" type="reset" class="btn btn-secondary">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
