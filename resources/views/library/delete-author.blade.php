@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Are you sure that you want to delete the author?</h4>
                                <h5><a href="{{route('author.show', $author->id)}}">{{$author->name}}</a></h5>
                            </div>
                        </div>

                        <div class="iq-card-body">
                            @if(count($author->books) > 0)
                                <div class="mb-3 d-block">
                                    <p>You cannot able to delete the author before delete the following books</p>
                                    <ul>
                                        @foreach($author->books as $book)
                                            <li><a href="{{route('book.show', $book->id)}}"> {{$book->title}} </a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('author.destroy', $author->id)}}" method="post">
                                @csrf()
                                <button type="submit" class="btn btn-danger">Delete</button>
                                <button onclick="window.location.href='{{route('author.index')}}'" type="reset" class="btn btn-secondary">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
