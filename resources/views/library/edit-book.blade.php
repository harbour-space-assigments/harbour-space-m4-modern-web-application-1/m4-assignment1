@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Edit Books</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form action="{{route('book.update', $book->id)}}" method="post">
                                @csrf()
                                <div class="form-group">
                                    <label>Book Title:</label>
                                    <input type="text" name="title" class="form-control" value="{{old('title', $book->title)}}">
                                </div>
                                <div class="form-group">
                                    <label>Book ISBN:</label>
                                    <input type="text" name="isbn" class="form-control" value="{{old('isbn', $book->isbn)}}">
                                </div>
                                <div class="form-group">
                                    <label>Book Author:</label>
                                    <select class="form-control" id="exampleFormControlSelect2" name="author_id">
                                        <option selected="" disabled="">Book Author</option>
                                        @foreach($authors as $author)
                                            <option {{old('author_id', $book->author_id) == $author->id ? "selected": ""}} value="{{$author->id}}">{{$author->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Book Publishing:</label>
                                    <input type="date" name="publishing" class="form-control" value="{{old('publishing', $book->publishing)}}">
                                </div>
                                <div class="form-group">
                                    <label>Book Pages:</label>
                                    <input type="number" name="pages" class="form-control" value="{{old('pages', $book->pages)}}">
                                </div>
                                <div class="form-group">
                                    <label>Book Price:</label>
                                    <input type="number" name="price" class="form-control" value="{{old('price', $book->price)}}">
                                </div>
                                <div class="form-group">
                                    <label>Book Rate:</label>
                                    <input type="number" name="rate" class="form-control" value="{{old('pages', $book->pages)}}">
                                </div>
                                <div class="form-group">
                                    <label>Book Description:</label>

                                    <textarea class="form-control" name="description" rows="4">{{old('description', $book->description)}}</textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button onclick="window.location.href='{{route('book.index')}}'" type="reset" class="btn btn-secondary">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
