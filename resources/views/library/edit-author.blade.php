@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Edit Author</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <form action="{{route('author.update', $author->id)}}" method="post">
                                @csrf()
                                <div class="form-group">
                                    <label>Author Name:</label>
                                    <input type="text" name="name" class="form-control" value="{{old('name', $author->name)}}">
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button onclick="window.location.href='{{route('author.index')}}'" type="reset" class="btn btn-secondary">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
