@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Author Lists</h4>
                            </div>
                            <div class="iq-card-header-toolbar d-flex align-items-center">
                                <a href="{{route('author.create')}}" class="btn btn-primary">Add New author</a>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            @include('components.search')
                            @if(count($authors) > 0)
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%;">
                                                <span>No</span>
                                                <span>{{{view('components.arrow', ['column' => 'id'])}}}</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Author Image</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Author Name</span>
                                                <span>{{{view('components.arrow', ['column' => 'name'])}}}</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Author Books</span>
                                                <span>{{{view('components.arrow', ['column' => 'books_count'])}}}</span>
                                            </th>
                                            <th style="width: 15%;">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($authors as $author)
                                            <tr>
                                                <td>{{$loop->index + 1}}</td>
                                                <td><img class="img-fluid rounded " style="width: 64px;" src="/storage/{{$author->image_path}}" alt=""></td>
                                                <td>{{$author->name}}</td>
                                                <td>{{$author->books_count}}</td>
                                                <td>
                                                    <div class="flex align-items-center list-user-action">
                                                        <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"
                                                           href="{{route('author.show', $author->id)}}"><i
                                                                class="ri-eye-line"></i></a>
                                                        @can('update-author', $author)
                                                            <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
                                                               href="{{route('author.edit', $author->id)}}"><i
                                                                    class="ri-pencil-line"></i></a>
                                                        @endcan
                                                        @can('update-author', $author)
                                                            <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                                                               href="{{route('author.delete', $author->id)}}"><i
                                                                    class="ri-delete-bin-line"></i></a>
                                                        @endcan
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @include('components.pagination', ['skip' => $skip, 'take' => $take, 'total' => $total])
                                </div>
                            @else
                                <h3> OPPS! No matching results </h3>
                                <a href="{{route('author.index')}}">Clear filters</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
