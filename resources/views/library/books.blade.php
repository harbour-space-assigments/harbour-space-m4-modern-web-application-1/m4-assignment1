@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Book Lists</h4>
                            </div>
                            <div class="iq-card-header-toolbar d-flex align-items-center">
                                <a href="{{route('book.create')}}" class="btn btn-primary">Add New book</a>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            @include('components.search')
                            @if(count($books) > 0)
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th style="width: 3%;">
                                                <span>No</span>
                                                <span>{{{view('components.arrow', [ 'column' => 'id'])}}}</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Image</span>
                                            </th>
                                            <th style="width: 8%;">
                                                <span>Title</span>
                                                <span>{{{view('components.arrow', [ 'column' => 'name'])}}}</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Author</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>ISBN</span>
                                                <span>{{{view('components.arrow', [ 'column' => 'isbn'])}}}</span>
                                            </th>
                                            <th style="width: 30%;">
                                                <span>Description</span>
                                                <span>{{{view('components.arrow', [ 'column' => 'description'])}}}</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Price</span>
                                                <span>{{{view('components.arrow', [ 'column' => 'price'])}}}</span>
                                            </th>
                                            <th style="width: 10%;">
                                                <span>Pages</span>
                                                <span>{{{view('components.arrow', [ 'column' => 'pages'])}}}</span>
                                            </th>
                                            <th style="width: 10px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($books as $book)
                                            <tr>
                                                <td>{{$loop->index + 1}}</td>
                                                <td><img class="img-fluid rounded" src="/storage/{{$book->preview_image_path}}" alt=""></td>
                                                <td>{{$book->title}}</td>
                                                <td>{{$book->author->name}}</td>
                                                <td>{{$book->isbn}}</td>
                                                <td>
                                                    <p class="mb-0">{{$book->description}}</p>
                                                </td>
                                                <td>${{$book->price}}</td>
                                                <td>{{$book->pages}} Pages</td>
                                                <td>
                                                    <div class="d-flex align-items-center list-user-action justify-content-center">
                                                        <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="{{route('book.show', $book->id)}}"><i
                                                                class="ri-eye-line"></i></a>

                                                        @can('update-book', $book)
                                                            <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
                                                               href="{{route('book.edit', $book->id)}}"><i
                                                                    class="ri-pencil-line"></i></a>
                                                        @endcan

                                                        @can('update-book', $book)
                                                            <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                                                               href="{{route('book.delete', $book->id)}}"><i
                                                                    class="ri-delete-bin-line"></i></a>
                                                        @endcan
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @include('components.pagination', ['skip' => $skip, 'take' => $take, 'total' => $total])
                                </div>
                            @else
                                <h3> OPPS! No matching results </h3>
                                <a href="{{route('book.index')}}">Clear filters</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
