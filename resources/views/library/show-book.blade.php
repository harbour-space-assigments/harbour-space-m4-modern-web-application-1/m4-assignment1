@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title mb-0">Books Description</h4>
                            @can('update-book', $book)
                                <div class="flex align-items-center list-user-action">
                                    <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="{{route('book.edit', $book->id)}}"><i
                                            class="ri-pencil-line"></i></a>
                                    <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="{{route('book.delete', $book->id)}}"><i
                                            class="ri-delete-bin-line"></i></a>
                                </div>
                            @endcan

                        </div>
                        <div class="iq-card-body pb-0">
                            <div class="description-contens align-items-top row">
                                <div class="col-md-4">
                                    <div class="iq-card-transparent iq-card-block iq-card-stretch iq-card-height">
                                        <div class="iq-card-body p-0">
                                            <div class="row align-items-center">
                                                <div class="col-3">
                                                    <ul id="description-slider-nav" class="list-inline p-0 m-0  d-flex align-items-center">
                                                        @foreach($book->images as $image)
                                                            <li>
                                                                <a href="/storage/{{$image->path}}">
                                                                    <img src="/storage/{{$image->thumbnail_path}}" class="img-fluid rounded w-100" alt="">
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="col-9">
                                                    <ul id="description-slider" class="list-inline p-0 m-0  d-flex align-items-center">
                                                        @foreach($book->images as $image)
                                                            <li>
                                                                <a href="/storage/{{$image->path}}">
                                                                    <img src="/storage/{{$image->path}}" class="img-fluid rounded w-100" alt="">
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="iq-card-transparent iq-card-block iq-card-stretch iq-card-height">
                                        <div class="iq-card-body p-0">
                                            <h3 class="mb-3">{{$book->title}}</h3>
                                            <div class="price d-flex align-items-center font-weight-500 mb-2">
                                                <span class="font-size-24 text-dark">${{$book->price}}</span>
                                            </div>
                                            <div class="mb-3 d-block">
                                          <span class="font-size-20 text-warning">
                                                @for($i=0; $i<$book->rate; $i++)
                                                  <i class="fa fa-star mr-1"></i>
                                              @endfor
                                          </span>
                                            </div>
                                            <span class="text-dark mb-4 pb-4 iq-border-bottom d-block">{{$book->description}}</span>
                                            <div class="text-primary mb-4">Author: <a href="{{route('author.show', $book->author->id)}}">
                                                    <span class="text-body">{{$book->author->name}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
