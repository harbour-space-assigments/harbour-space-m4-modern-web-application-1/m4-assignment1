@extends('layouts.library')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title mb-0">Author Information</h4>
                            @can('update-author', $author)
                                <div class="flex align-items-center list-user-action">
                                    <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="{{route('author.edit', $author->id)}}"><i
                                            class="ri-pencil-line"></i></a>
                                    <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="{{route('author.delete', $author->id)}}"><i
                                            class="ri-delete-bin-line"></i></a>
                                </div>
                            @endcan
                        </div>
                        <div class="iq-card-body pb-0">
                            <div class="description-contens align-items-top row">
                                <div class="col-md-auto">
                                    <div class="iq-card-transparent iq-card-block iq-card-stretch iq-card-height">
                                        <div class="iq-card-body p-0">
                                            <div class="row align-items-center">
                                                <a href="javascript:void(0);">
                                                    <img src="/storage/{{$author->image_path}}" class="img-fluid rounded" style="width: 128px;" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="iq-card-transparent iq-card-block iq-card-stretch iq-card-height">
                                        <div class="iq-card-body p-0">
                                            <h3 class="mb-3">{{$author->name}}</h3>
                                            <div class="mb-3 d-block">
                                                <b>Author Books:</b>
                                                <ul>
                                                    @foreach($author->books as $book)
                                                        <li><a href="{{route('book.show', $book->id)}}"> {{$book->title}} </a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
