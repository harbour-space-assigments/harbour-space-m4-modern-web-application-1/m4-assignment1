<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Home') }}
        </h2>
    </x-slot>

    <div class="container-fluid">
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <a href="{{route('weather')}}">
                        <div class="p-6 bg-white border-b border-gray-200">
                            {{__('Weather')}}
                        </div>
                    </a>

                    <a href="{{route('book.index')}}">
                        <div class="p-6 bg-white border-b border-gray-200">
                            {{__('Books')}}
                        </div>
                    </a>

                    <a href="{{route('author.index')}}">
                        <div class="p-6 bg-white border-b border-gray-200">
                            {{__('Authors')}}
                        </div>
                    </a>

                </div>

                <div class="justify-content-center d-flex mt-5">
                    <h1 class="text-center " style="font-size: 36px; font-weight: bold">Donate to the Library</h1>
                    <p style="font-size: 20px;">Libraries are much more than buildings full of books; they are community gathering places that serve as learning centers for all educational levels,
                        professional development,
                        culture, creativity and fun. Support from the community is vital to the success of the Library and there are several ways to give that will help in the purchase of books,
                        magazines, electronic resources and new technology. Financial donations also support the many programs the Library offers throughout the year.</p>
                    <div class="d-flex justify-content-center w-100 text-center mt-5">
                        <div id="donate-button-container">
                            <div id="donate-button"></div>
                            <script src="https://www.paypalobjects.com/donate/sdk/donate-sdk.js" charset="UTF-8"></script>
                            <script>
                                PayPal.Donation.Button({
                                    env: 'production',
                                    hosted_button_id: 'VR8TEL53N8DS6',
                                    image: {
                                        src: 'https://www.paypalobjects.com/en_US/ES/i/btn/btn_donateCC_LG.gif',
                                        alt: 'Donate with PayPal button',
                                        title: 'PayPal - The safer, easier way to pay online!',
                                    }
                                }).render('#donate-button');
                            </script>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</x-app-layout>
