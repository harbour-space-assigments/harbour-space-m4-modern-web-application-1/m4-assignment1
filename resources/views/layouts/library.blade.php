<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>HS - Assigment 3</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('/library/images/favicon.ico')}}"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/library/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/library/css/dataTables.bootstrap4.min.css')}}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{{asset('/library/css/typography.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('/library/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('/library/css/responsive.css')}}">
</head>
<body>
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">
    </div>
</div>
<!-- loader END -->
<!-- Wrapper Start -->
<div class="wrapper">
    <div class="container">
        @if(!empty(session('message')))
            <div class="alert alert-success mt-5 ml-1 mr-1" role="alert">{{session('message')}} </div>
        @endif
        @if(!empty(session('error')))
            <div class="alert alert-danger mt-5 ml-1 mr-1" role="alert">{{session('error')}} </div>
        @endif
        @if(!empty(session('errors')))
            <div class="mt-5">
                @foreach(json_decode(session('errors')) as $field => $errors)
                    <div class="alert alert-danger mt-1 ml-1 mr-1" role="alert"><b>{{(ucfirst($field))}}</b>: {{$errors[0]}} </div>
                @endforeach
            </div>
        @endif
    </div>


    @yield('content')
</div>

<footer class="iq-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-right">
                Copyright 2020 <a href="#">Booksto</a> All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- Footer END -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('/library/js/jquery.min.js')}}"></script>
<script src="{{asset('/library/js/popper.min.js')}}"></script>
<script src="{{asset('/library/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/library/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/library/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- Appear JavaScript -->
<script src="{{asset('/library/js/jquery.appear.js')}}"></script>
<!-- Countdown JavaScript -->
<script src="{{asset('/library/js/countdown.min.js')}}"></script>
<!-- Counterup JavaScript -->
<script src="{{asset('/library/js/waypoints.min.js')}}"></script>
<script src="{{asset('/library/js/jquery.counterup.min.js')}}"></script>
<!-- Wow JavaScript -->
<script src="{{asset('/library/js/wow.min.js')}}"></script>
<!-- Apexcharts JavaScript -->
<script src="{{asset('/library/js/apexcharts.js')}}"></script>
<!-- Slick JavaScript -->
<script src="{{asset('/library/js/slick.min.js')}}"></script>
<!-- Select2 JavaScript -->
<script src="{{asset('/library/js/select2.min.js')}}"></script>
<!-- Owl Carousel JavaScript -->
<script src="{{asset('/library/js/owl.carousel.min.js')}}"></script>
<!-- Magnific Popup JavaScript -->
<script src="{{asset('/library/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Smooth Scrollbar JavaScript -->
<script src="{{asset('/library/js/smooth-scrollbar.js')}}"></script>
<!-- lottie JavaScript -->
<script src="{{asset('/library/js/lottie.js')}}"></script>
<!-- am core JavaScript -->
<script src="{{asset('/library/js/core.js')}}"></script>
<!-- am charts JavaScript -->
<script src="{{asset('/library/js/charts.js')}}"></script>
<!-- am animated JavaScript -->
<script src="{{asset('/library/js/animated.js')}}"></script>
<!-- am kelly JavaScript -->
<script src="{{asset('/library/js/kelly.js')}}"></script>
<!-- am maps JavaScript -->
<script src="{{asset('/library/js/maps.js')}}"></script>
<!-- am worldLow JavaScript -->
<script src="{{asset('/library/js/worldLow.js')}}"></script>
<!-- Raphael-min JavaScript -->
<script src="{{asset('/library/js/raphael-min.js')}}"></script>
<!-- Morris JavaScript -->
<script src="{{asset('/library/js/morris.js')}}"></script>
<!-- Morris min JavaScript -->
<script src="{{asset('/library/js/morris.min.js')}}"></script>
<!-- Flatpicker Js -->
<script src="{{asset('/library/js/flatpickr.js')}}"></script>
<!-- Style Customizer -->
<script src="{{asset('/library/js/style-customizer.js')}}"></script>
<!-- Chart Custom JavaScript -->
<script src="{{asset('/library/js/chart-custom.js')}}"></script>
<!-- Custom JavaScript -->
<script src="{{asset('/library/js/custom.js')}}"></script>
</body>
</html>
