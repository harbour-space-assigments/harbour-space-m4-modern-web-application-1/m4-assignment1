@php
    function toC($k){return $k -273.15; }
    function getDir($th){
        if($th >= 0 && $th <90)
            return "East";
        if($th >= 90 && $th < 180)
            return "North";
        if($th >= 180 && $th <270)
            return "West";
        if($th >= 270 && $th <360)
            return "South";

        return null;
    }
@endphp
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>HS Assigment</title>

    <!-- Loading third party fonts -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
    <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Loading main css file -->
    <link rel="stylesheet" href="style.css">

    <!--[if lt IE 9]>
    <script src="js/ie-support/html5.js"></script>
    <script src="js/ie-support/respond.js"></script>
    <![endif]-->

</head>

<body>

<div class="site-content">
    <div class="site-header">
        <div class="container">
            <a href="/" class="branding">
                <img src="images/logo.png" alt="" class="logo">
                <div class="logo-type">
                    <h1 class="site-title">HS Assigment</h1>
                </div>
            </a>

            <!-- Default snippet for navigation -->
            <div class="main-navigation">
                <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                <ul class="menu">
                    <li class="menu-item current-menu-item"><a href="/">Home</a></li>
                    <li class="menu-item"><a href="/weather-table">Weather Table </a></li>

                </ul> <!-- .menu -->
            </div> <!-- .main-navigation -->

            <div class="mobile-navigation"><ul class="menu">
                    <li class="menu-item current-menu-item"><a href="index.html">Home</a></li>
                    <li class="menu-item"><a href="/weather-table">Weather Table</a></li>
                </ul>
            </div>

        </div>
    </div>

    <div class="hero" data-bg-image="images/banner.png">
        <div class="container">
            <form action="weather" class="find-location">
                @csrf()
                <input name="q" type="text" placeholder="Find your location..." value="{{old('q', $q ?? "Spain")}}">
                <input type="submit" value="Find">
            </form>

        </div>
    </div>
    <div class="forecast-table">
        <div class="container">
            <div class="forecast-container">
                <div class="today forecast">
                    <div class="forecast-header">
                        <div class="day">{{ date('l')}}</div>
                        <div class="date">{{ date('d M')}}</div>
                    </div> <!-- .forecast-header -->
                    <div class="forecast-content">
                        <div class="location">{{$data->name}}</div>
                        <div class="degree">
                            <div class="num">{{round(toC($data->main->temp), 1)}}<sup>o</sup>C</div>
                            <div class="forecast-icon" style="margin-bottom:66px;">
                                <img src="http://openweathermap.org/img/wn/{{$data->weather[0]->icon}}.png" alt="" width=90>
                            </div>
                        </div>
                        @if(isset($data->wind) && isset($data->wind->gust))
                            <span><img src="images/icon-umberella.png" alt="">{{round($data->wind->gust, 2)}}%</span>
                        @endif
                        @if(isset($data->wind) && isset($data->wind->speed))
                            <span><img src="images/icon-wind.png" alt="">{{round($data->wind->speed, 2)}}km/h</span>
                        @endif
                        @if(isset($data->wind) && isset($data->wind->deg))
                            <span><img src="images/icon-compass.png" alt="">{{getDir($data->wind->deg)}}</span>
                        @endif
                    </div>
                </div>
                @foreach($daily as $day)
                    @if(!$loop->last && !$loop->first)
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">{{date('D', $day->dt)}}</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="http://openweathermap.org/img/wn/{{$day->weather[0]->icon}}.png" alt="" width=48>
                                </div>
                                <div class="degree">{{round(toC($day->temp->day), 1)}}<sup>o</sup>C</div>
                                <small>{{round(toC($day->temp->night), 1)}}<sup>o</sup></small>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <form action="#" method="post" class="subscribe-form">
                        <input type="text" placeholder="" style="visibility: hidden">
                    </form>
                </div>
                <div class="col-md-3 col-md-offset-1">
                </div>
            </div>

            <p class="colophon">Copyright 2021 Harbour.Space. Designed by Adel in 60 min, All rights reserved</p>
        </div>
    </footer> <!-- .site-footer -->
</div>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>

</body>

</html>
