<div id="{{$images_name}}-items">
    <div class="{{$images_name}}-item">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <input class="form-control" type="text" name="{{$descriptions_name}}[]" placeholder="Description">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <div class="custom-file">
                        <label class="custom-file-label">File</label>
                        <input class="custom-file-input" type="file" name="{{$images_name}}[]">
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="flex align-items-center list-user-action">
                    <a class="remove-item d-flex bg-danger justify-content-center align-items-center" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                       href="javascript:void(0)"
                       style="width: 64px; height: 42px">
                        <i class="ri-delete-bin-line" style="font-size: 18px"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-primary" type="button" onclick="handleAddImageClick(this)">Add Image</button>

<script>
    const handleAddImageClick = (event) => {
        addImageInput();
    };

    const addImageInput = () => {
        const itemsContainer = document.getElementById("{{$images_name}}-items");
        const item = itemsContainer.querySelector('div.{{$images_name}}-item').cloneNode(true);

        item.querySelectorAll("input").forEach(input => {
            input.value = "";
        });

        item.querySelector("input[type=file]").onchange = (event) => {
            item.querySelector("label.custom-file-label").innerHTML = event.target.files[0].name;
        };
        item.querySelector("label.custom-file-label").innerHTML = "File";
        item.querySelector("a.remove-item").onclick = () => {
            if (itemsContainer.children.length > 1)
                item.parentElement.removeChild(item);
        };

        itemsContainer.appendChild(item);
    };


    addImageInput();
    const firstItem = document.querySelector('#{{$images_name}}-items div.{{$images_name}}-item');
    firstItem.parentElement.removeChild(firstItem);
</script>
