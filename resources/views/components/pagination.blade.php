@php
    $page = $skip/$take + 1;
    $last = $total/$take;
@endphp
<div class="flex align-items-center list-user-action">
    <a class="bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Previews"
       href="{{request()->fullUrlWithQuery(['skip' => max(0, $skip - $take)]) }}">
        <i class="ri-arrow-left-line"></i>
    </a>

    @if($page != 1)
        <a class="bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="First" href="{{request()->fullUrlWithQuery(['skip' => 0]) }}">1</a>
        ..
    @endif

    <a class="bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Current">
        {{$page}}
    </a>

    @if($page != $last)
        ..
        <a class="bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Last" href="{{request()->fullUrlWithQuery(['skip' => ($last-1) * $take]) }}">{{$last}}</a>
    @endif

    <a class="bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Next"
       href="{{request()->fullUrlWithQuery(['skip' => min(($last-1) * $take, $skip + $take)]) }}"><i
            class="ri-arrow-right-line"></i>
    </a>

    <span class="ml-5"> Page Size: </span>

    @foreach(['5', '10', '25', '50'] as $size)
        <a class="bg-{{$size == $take ? 'primary' : 'secondary'}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$size}}" href="{{request()->fullUrlWithQuery(['take' => $size]) }}">
            {{$size}}
        </a>
    @endforeach
</div>
