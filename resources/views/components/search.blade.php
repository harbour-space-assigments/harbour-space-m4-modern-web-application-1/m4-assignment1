@php
    $q = request()->input('q');
@endphp

<form action="{{request()->fullUrl()}}" method="get" class="subscribe-form">
    <label>Search:</label>
    <div class="row">
        <div class="col-2">
            <div class="form-group">
                <input class="form-control" style="height: 35px;" name="q" value="{{$q}}" placeholder="">
            </div>
        </div>
        <div class="col-2">
            <button class="btn btn-primary" type="submit">Search</button>
        </div>
    </div>
</form>
