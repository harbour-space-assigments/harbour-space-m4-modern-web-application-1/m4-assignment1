@php
    $sorted_column = request()->input('sort_by');
    $sorted_order = request()->input('sort_order');

    $hide_up = $column == $sorted_column && $sorted_order == "asc";
    $hide_down = $column == $sorted_column && $sorted_order == "desc";
    $route_up = request()->fullUrlWithQuery(['sort_by' => $column, 'sort_order'=>'asc']);
    $route_down = request()->fullUrlWithQuery(['sort_by' => $column, 'sort_order'=>'desc']);
@endphp
@if(!$hide_up)
    <a class="bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="ASC" href="{{$route_up}}">
        <i class="ri-arrow-up-fill"></i>
    </a>
@endif
@if(!$hide_down)
    <a class="bg-secondary" data-toggle="tooltip" data-placement="top" title="" data-original-title="DESC" href="{{$route_down}}">
        <i class="ri-arrow-down-fill"></i>
    </a>
@endif
