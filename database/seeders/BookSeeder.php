<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Image;
use Database\Factories\ImageFactory;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::factory(20)->has(Image::factory()->count(5))->create();
    }
}
