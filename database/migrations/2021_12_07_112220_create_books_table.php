<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('description');
            $table->string('isbn')->index();
            $table->unsignedBigInteger('author_id');
            $table->unsignedInteger('price');
            $table->unsignedInteger('pages');
            $table->unsignedInteger('rate');
            $table->date('publishing')->nullable();
            $table->timestamps();

            $table->foreign('author_id')->on('authors')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
