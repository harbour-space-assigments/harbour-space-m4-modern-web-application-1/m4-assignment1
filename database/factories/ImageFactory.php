<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'path' => Storage::disk('public')->putFile('/', new File($this->faker->image(null, 640, 480, 'business'))),
            'thumbnail_path' => Storage::disk('public')->putFile('/', new File($this->faker->image(null, 128, 96, 'business'))),
            'description' => $this->faker->text(),
        ];
    }
}
