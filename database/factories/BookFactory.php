<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence($this->faker->numberBetween(2, 5)),
            'isbn' => $this->faker->isbn10(),
            'author_id' => $this->faker->randomElement(Author::all())->id,
            'owner_id' => $this->faker->randomElement(User::all())->id,
            'pages' => $this->faker->randomNumber(3),
            'price' => $this->faker->randomNumber(3),
            'description' => $this->faker->sentence($this->faker->numberBetween(12, 40)),
            'rate' => $this->faker->numberBetween(1, 5),
            'publishing' => $this->faker->date(),
        ];
    }
}
