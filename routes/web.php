<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/weather', [Controller::class, 'weather'])->name('weather');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [Controller::class, 'home'])->name('home');

    Route::group(['prefix' => '/books', 'as' => 'book.'], function () {
        Route::get('/', [BookController::class, 'index'])->name('index');
        Route::get('/create', [BookController::class, 'create'])->name('create');
        Route::get('/{id}', [BookController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BookController::class, 'edit'])->name('edit');
        Route::get('/{id}/delete', [BookController::class, 'delete'])->name('delete');

        Route::post('/', [BookController::class, 'store'])->name('store');
        Route::post('/{id}/update', [BookController::class, 'update'])->name('update');
        Route::post('/{id}/delete', [BookController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => '/authors', 'as' => 'author.'], function () {
        Route::get('/', [AuthorController::class, 'index'])->name('index');
        Route::get('/create', [AuthorController::class, 'create'])->name('create');
        Route::get('/{id}', [AuthorController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [AuthorController::class, 'edit'])->name('edit');
        Route::get('/{id}/delete', [AuthorController::class, 'delete'])->name('delete');

        Route::post('/', [AuthorController::class, 'store'])->name('store');
        Route::post('/{id}/update', [AuthorController::class, 'update'])->name('update');
        Route::post('/{id}/delete', [AuthorController::class, 'destroy'])->name('destroy');
    });

});

require __DIR__ . '/auth.php';
