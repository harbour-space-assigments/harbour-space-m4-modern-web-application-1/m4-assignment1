<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'isbn', 'author_id', 'price', 'rate', 'pages', 'publishing'];

    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }


    public function getPreviewImagePathAttribute()
    {
        return @$this->images()->first()->path ?? $this->defaultImage();
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'book_id', 'id');
    }

    public function defaultImage()
    {
        return "";
    }
}
