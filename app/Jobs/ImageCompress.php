<?php

namespace App\Jobs;

use App\Models\Image as BookImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageCompress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $image, $width, $quality;

    /**
     * Create a new job instance.
     *
     * @param BookImage $image
     * @param int $width
     * @param int $quality
     */
    public function __construct(BookImage $image, $width = 320, $quality = 75)
    {
        $this->image = $image;
        $this->width = $width;
        $this->quality = $quality;

        assert($this->width > 0, 'The image width cannot be non positive');
        assert($this->quality > 0 && $this->quality <= 100, 'The quality should be between 1 and 100');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $image = $this->image;
        $compressed_image = Image::make(Storage::disk('public')->get($image->path))->resize($this->width, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', $this->quality);

        $path = Str::uuid() . '.jpg';
        Storage::disk('public')->put($path, $compressed_image);
        $image->thumbnail_path = $path;

        $image->push();
    }

}
