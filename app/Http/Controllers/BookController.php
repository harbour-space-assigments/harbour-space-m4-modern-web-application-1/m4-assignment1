<?php

namespace App\Http\Controllers;

use App\Jobs\ImageCompress;
use App\Models\Author;
use App\Models\Book;
use App\Models\BookImage;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function index(Request $request)
    {
        $take = $request->input('take', 10);
        $skip = $request->input('skip', 0);
        $sort_by = $request->input('sort_by', 'id');
        $sort_order = $request->input('sort_order', 'asc');
        $q = $request->input('q');

        $skip = max(0, $skip);

        if (!in_array($sort_by, ['id', 'title', 'isbn', 'author_id', 'price', 'rate', 'pages', 'publishing'])) {
            $sort_by = 'id';
            $sort_order = 'asc';
        }

        if (!in_array($sort_order, ['asc', 'desc']))
            $sort_order = 'asc';

        $books = Book::query()->with('author');

        if ($q)
            $books = $books->where('title', 'like', "%$q%");

        $books = $books->orderBy($sort_by, $sort_order)->take($take)->skip($skip)->get();
        return view('library.books')->with(['books' => $books, 'skip' => $skip, 'take' => $take, 'total' => Book::query()->count()]);
    }

    public function show(Request $request, $id)
    {
        $book = Book::query()->with('images')->find($id);

        if (empty($book))
            return $this->response404();

        return view('library.show-book')->with(['book' => $book]);
    }

    public function create()
    {
        $authors = Author::query()->select(['id', 'name'])->get();
        return view('library.create-book')->with(['authors' => $authors]);
    }

    public function edit(Request $request, $id)
    {
        $book = Book::query()->find($id);

        if (empty($book))
            return $this->response404();

        if (Gate::denies('update-book', $book))
            return $this->response403();


        $authors = Author::query()->select(['id', 'name'])->get();
        return view('library.edit-book')->with(['book' => $book, 'authors' => $authors]);
    }

    public function delete(Request $request, $id)
    {
        $book = Book::query()->find($id);

        if (empty($book))
            return $this->response404();

        if (Gate::denies('update-book', $book))
            return $this->response403();

        return view('library.delete-book')->with(['book' => $book]);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required|string|min:2|max:255',
            'isbn' => 'required|string|unique:books,id',
            'author_id' => 'required|exists:authors,id',
            'price' => 'required|numeric',
            'rate' => 'required|numeric|min:1|max:5',
            'pages' => 'required|numeric',
            'publishing' => 'sometimes|nullable|string',
            'images' => 'required|array',
            'images.*' => 'required|image|max:4096|mimes:jpeg,jpg,png,gif',
            'descriptions' => 'required|array',
            'descriptions.*' => 'required|string|max:512',
        ]);

        if ($validation->fails())
            return redirect()->route('book.create')->with('errors', $validation->errors());

        $book = new Book($request->all());
        $book->owner_id = Auth::id();
        $book->save();

        $images = $request->file('images', []);
        $descriptions = $request->input('descriptions', []);

        $book->images()->createMany(
            collect($images)->zip($descriptions)->map(function ($item) {
                return new Image([
                    'path' => Storage::disk('public')->putFile('/', $item[0]),
                    'description' => $item[1],
                ]);
            })->toArray()
        );

        $book->images()->each(function ($image) {
            ImageCompress::dispatchSync($image);
        });

        return redirect()->route('book.index')->with('message', 'The book has been added successfully');
    }

    public function update(Request $request, $id)
    {
        $book = Book::query()->find($id);

        if (empty($book))
            return $this->response404();

        if (Gate::denies('update-book', $book))
            return $this->response403();


        $validation = Validator::make($request->all(), [
            'title' => 'required|string|min:2|max:255',
            'isbn' => 'required|string|unique:books,id,' . $book->id,
            'author_id' => 'required|exists:authors,id',
            'price' => 'required|numeric',
            'rate' => 'required|numeric|min:1|max:5',
            'pages' => 'required|numeric',
            'publishing' => 'sometimes|nullable|string',
            'images' => 'sometimes|nullable|array',
            'images.*' => 'required|image|max:4096|mimes:jpeg,jpg,png,gif',
            'descriptions' => 'sometimes|nullable|array',
            'descriptions.*' => 'required|string|max:512',
        ]);


        if ($validation->fails())
            return redirect()->route('book.edit', $book->id)->with('errors', $validation->errors());

        $images = $request->file('images', []);
        $descriptions = $request->input('descriptions', []);

        $book->update($request->all());

        foreach ($book->images() as $image){
            Storage::disk('public')->delete($image->path);
            Storage::disk('public')->delete($image->thumbnail_path);
            $book->delete();
        }

        $book->images()->createMany(
            collect($images)->zip($descriptions)->map(function ($item) {
                return new Image([
                    'path' => Storage::disk('public')->putFile('/', $item[0]),
                    'description' => $item[1],
                ]);
            })->toArray()
        );

        $book->images()->each(function ($image) {
            ImageCompress::dispatchSync($image);
        });

        return redirect()->route('book.show', $book->id)->with('message', 'The book has been updated');
    }

    public function destroy(Request $request, $id)
    {
        $book = Book::query()->find($id);

        if (empty($book))
            return $this->response404();

        if (Gate::denies('update-book', $book))
            return $this->response403();

        foreach ($book->images() as $image){
            Storage::disk('public')->delete($image->path);
            Storage::disk('public')->delete($image->thumbnail_path);
            $book->delete();
        }

        $deleted = $book->delete();

        if ($deleted)
            return redirect()->route('book.index')->with('message', 'The book has been deleted');

        return redirect()->route('book.show', $book->id)->with('error', 'The book has not been deleted');
    }

}
