<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
        $take = $request->input('take', 10);
        $skip = $request->input('skip', 0);
        $sort_by = $request->input('sort_by', 'id');
        $sort_order = $request->input('sort_order', 'asc');

        if (!in_array($sort_by, ['id', 'name', 'books_count'])) {
            $sort_by = 'id';
            $sort_order = 'asc';
        }

        if (!in_array($sort_order, ['asc', 'desc']))
            $sort_order = 'asc';

        $authors = Author::query()->withCount('books')->orderBy($sort_by, $sort_order)->take($take)->skip($skip)->get();
        return view('library.authors')->with(['authors' => $authors, 'skip' => $skip, 'take' => $take, 'total' => Author::query()->count()]);
    }

    public function show(Request $request, $id)
    {
        $author = Author::query()->with('books')->find($id);

        if (empty($author))
            return $this->response404();

        return view('library.show-author')->with(['author' => $author]);
    }


    public function create()
    {
        $authors = Author::query()->select(['id', 'name'])->get();
        return view('library.create-author')->with(['authors' => $authors]);
    }

    public function edit(Request $request, $id)
    {
        $author = Author::query()->find($id);
        if (empty($author))
            return $this->response404();

        if (Gate::denies('update-author', $author))
            return $this->response403();

        $authors = Author::query()->select(['id', 'name'])->get();
        return view('library.edit-author')->with(['author' => $author, 'authors' => $authors]);
    }

    public function delete(Request $request, $id)
    {
        $author = Author::query()->find($id);
        if (empty($author))
            return $this->response404();

        if (Gate::denies('update-author', $author))
            return $this->response403();

        return view('library.delete-author')->with(['author' => $author]);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|min:2|max:255|unique:authors,name',
        ]);

        if ($validation->fails())
            return redirect()->route('author.create')->with('errors', $validation->errors());

        $author = new Author($request->all());
        $author->user_id = Auth::id();
        $author->save();

        return redirect()->route('author.index')->with('message', 'The author has been added successfully');
    }

    public function update(Request $request, $id)
    {
        $author = Author::query()->find($id);

        if (empty($author))
            return $this->response404();

        if (Gate::denies('update-author', $author))
            return $this->response403();

        $validation = Validator::make($request->all(), [
            'name' => 'required|string|min:2|max:255|unique:authors,name,' . $author->id,
        ]);

        if ($validation->fails())
            return redirect()->route('author.edit', $author->id)->with('errors', $validation->errors());

        $author->update($request->all());

        return redirect()->route('author.show', $author->id)->with('message', 'The author has been updated');
    }

    public function destroy(Request $request, $id)
    {
        $author = Author::query()->find($id);

        if (empty($author))
            return $this->response404();

        if (Gate::denies('update-author', $author))
            return $this->response403();

        if (Auth::user()->is_admin) {
            foreach ($author->books as $book) {
                foreach ($book->images as $image) {
                    Storage::disk('public')->delete($image->path);
                    Storage::disk('public')->delete($image->thumbnail_path);
                    $image->delete();
                }

                $book->delete();
            }
        } else if (!empty($author->books))
            return redirect()->route('author.delete', $author->id)->with('error', 'You should delete the author\'s books before delete the author');

        if ($author->delete())
            return redirect()->route('author.index')->with('message', 'The author has been deleted');

        return redirect()->route('author.show', $author->id)->with('error', 'The author has not been deleted');
    }

}
