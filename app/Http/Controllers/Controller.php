<?php

namespace App\Http\Controllers;

use App\Services\WeatherService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $weatherService = null;

    public function __construct()
    {
        $this->weatherService = new WeatherService();
    }

    public function weather(Request $request)
    {
        $q = $request->input('q', 'Spain');

        try {
            $weather = $this->weatherService->getWeather($q);
            $forecast = $this->weatherService->getForecast($weather->coord->lat, $weather->coord->lon);

            $daily = $forecast->daily;

            return view('weather', ['data' => $weather, 'daily' => $daily, 'q' => $q]);
        } catch (\Exception $exception) {
            return view('500', ['message' => $exception->getMessage()]);
        }
    }

    public function weatherTable(Request $request)
    {
        $q = $request->input('q', 'Spain');
        $sort_by = $request->input('sort_by');
        $sort_order = $request->input('sort_order', 'asc');


        try {
            $weather = $this->weatherService->getWeather($q);
            $forecast = $this->weatherService->getForecast($weather->coord->lat, $weather->coord->lon);

            $daily = collect($forecast->daily)->sortBy(function ($day) use ($sort_by) {
                switch ($sort_by) {
                    case 'day':
                        return $day->dt;
                    case 'temperature-day':
                        return $day->temp->day;
                    case 'temperature-night':
                        return $day->temp->night;
                    case 'clouds':
                        return @$day->clouds ?? 0;
                    case 'rain':
                        return @$day->rain ?? 0;

                    default:
                        return null;
                }
            }, SORT_REGULAR, $sort_order == 'desc')->toArray();

            return view('weather-table', ['data' => $weather, 'daily' => $daily, 'q' => $q]);
        } catch (\Exception $exception) {
            return view('500', ['message' => $exception->getMessage()]);
        }
    }

    public function response404()
    {
        abort(404);
    }

    public function response403()
    {
        abort(403);
    }


    public function home(){
        return view('home');
    }
}
