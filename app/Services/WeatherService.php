<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;

class WeatherService
{

    protected $client = null;

    public function __construct()
    {
        $handler = new HandlerStack();
        $handler->setHandler(new CurlHandler());

        $handler->unshift(Middleware::mapRequest(function(RequestInterface $request) {
            return $request->withUri(Uri::withQueryValue($request->getUri(), 'appid', env('WEATHER_API_KEY')));
        }));

        $this->client = new Client([
            'base_uri' => env('WEATHER_BASE_URI'),
            'handler' => $handler,
        ]);
    }

    public function getWeather($q = "")
    {
        $response = $this->client->get('weather', [
            'query' => [
                'q' => $q,
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function getForecast($lat, $lon)
    {
        $response = $this->client->get('onecall', [
            'query' => [
                'lat' => $lat,
                'lon' => $lon
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }


}
