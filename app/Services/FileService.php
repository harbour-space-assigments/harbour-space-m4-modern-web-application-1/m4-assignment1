<?php

namespace App\Services;

use App\Models\File;
use Illuminate\Http\UploadedFile;

class FileService
{

    public function __construct()
    {

    }

    static function createImageFile(UploadedFile $image): File
    {
        return new File([
            'type' => 'image',
            'filename' => $image->getClientOriginalName(),
            'extension' => $image->getExtension(),
            'size' => $image->getSize(),
            'hash' => md5($image),
            'path' => $image->storePublicly('images'),
        ]);}


}
